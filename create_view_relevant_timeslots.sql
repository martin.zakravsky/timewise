CREATE VIEW `relevant_timeslots` AS

SELECT `users_jobs_e`.`user_id` AS `user_id`,`timeslots`.`id` AS `timeslot_id`,`timeslots`.`date` AS `timeslot_date`,`jobs`.`project_id` AS `project_id`,`users_jobs_e`.`job_id` AS `job_id`,1 AS `asEmployee` FROM ((`users_jobs_e` JOIN `timeslots` ON((`timeslots`.`job_id` = `users_jobs_e`.`job_id`))) LEFT JOIN `jobs` ON((`jobs`.`id` = `users_jobs_e`.`job_id`)))

UNION

SELECT `users_jobs_s`.`user_id` AS `user_id`,`timeslots`.`id` AS `timeslot_id`,`timeslots`.`date` AS `timeslot_date`,`jobs`.`project_id` AS `project_id`,`users_jobs_s`.`job_id` AS `job_id`,0 AS `asEmployee` FROM ((`users_jobs_s` JOIN `timeslots` ON((`timeslots`.`job_id` = `users_jobs_s`.`job_id`))) LEFT JOIN `jobs` ON((`jobs`.`id` = `users_jobs_s`.`job_id`)))
