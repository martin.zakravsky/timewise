#!/bin/bash

echo " * Resetting schema and emptying database..."

./reset_schema.sh

echo " * Creating users..."

php bin/console fos:user:create john john@example.com password01
php bin/console fos:user:create anne anne@example.com password01
php bin/console fos:user:create luke luke@example.com password01

echo " * Creating dummy users..."

for i in {1..50}
    do
    php bin/console fos:user:create dummyuser$i dummyuser$i@example.com password01
    php bin/console fos:user:activate dummyuser$i
    done

echo " * Activating users..."

php bin/console fos:user:activate john
php bin/console fos:user:activate anne
php bin/console fos:user:activate luke

echo " * Promoting admins..."

php bin/console fos:user:promote john ROLE_ADMIN

echo " * Feeding other prepared data..."

mysql -u root -ppassword01 timewise < "testing_data.sql"

echo "* Creating auto shift switcher script...";

mysql -u root -ppassword01 timewise < "mark_shifts_as_finished.sql"

echo " * Done! "
