USE timewise;
SET GLOBAL event_scheduler="ON"
CREATE DEFINER=`root`@`localhost` EVENT `mark_shifts_as_finished` ON SCHEDULE EVERY 10 SECOND STARTS '2017-04-13 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE shifts LEFT JOIN timeslots ON shifts.timeslot_id=timeslots.id SET state = '5' WHERE timeslots.date < NOW() AND shifts.state = '1'
