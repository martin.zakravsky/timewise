echo " ** Clearing cache";
doctrine:cache:clear-metadata;
doctrine:cache:clear-query;
doctrine:cache:clear-result;
echo " ** Dropping schema";
php bin/console doctrine:schema:drop --force
echo " ** Generating entities";
php bin/console doctrine:generate:entities AppBundle
echo " ** Creating schema";
php bin/console doctrine:schema:create
echo " ** Updating schema";
php bin/console doctrine:schema:update --force
