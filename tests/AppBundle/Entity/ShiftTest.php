<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Entity\User;
use AppBundle\Entity\Job;
use AppBundle\Entity\Project;
use AppBundle\Entity\Shift;
use AppBundle\Entity\Timeslot;

class ShiftTest extends WebTestCase
{
    public function testSupervisorAutomaticConfirm()
    {
        // inicializace

        $user = new User();

        $timeslot = new Timeslot();

        $job = new Job();

        // test automatickeho potvrzeni koordinatorske smeny

        $timeslot -> setJob($job);

        $user -> addSupervising($job);

        $shift = new Shift($user, $timeslot);

        $this -> assertEquals(Shift::CONFIRMED, $shift -> getState());

        $user -> removeSupervising($job);

        // test spravneho chovani po odebrani koordinatorskych prav

        $shift2 = new Shift($user, $timeslot);

        $this -> assertEquals(Shift::UNCONFIRMED, $shift2 -> getState());
    }

    public function testShiftInFinalState() {
        $user = new User();
        $timeslot = new Timeslot();

        $shifts = new ArrayCollection();
        $shifts[] = (new Shift($user, $timeslot)) -> setState(Shift::UNCONFIRMED);
        $shifts[] = (new Shift($user, $timeslot)) -> setState(Shift::CONFIRMED);
        $shifts[] = (new Shift($user, $timeslot)) -> setState(Shift::REJECTED);
        $shifts[] = (new Shift($user, $timeslot)) -> setState(Shift::CANCELLED);
        $shifts[] = (new Shift($user, $timeslot)) -> setState(Shift::REQUIRES_CANCELLATION);
        $shifts[] = (new Shift($user, $timeslot)) -> setState(Shift::MISSED);
        $shifts[] = (new Shift($user, $timeslot)) -> setState(Shift::FINISHED);

        $this -> assertEquals(4, $shifts -> filter(function($item) {
            return $item -> isInFinalState() == true;
        }) -> count());

    }
}
