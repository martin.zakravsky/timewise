<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Entity\User;
use AppBundle\Entity\Job;
use AppBundle\Entity\Project;
use AppBundle\Entity\Shift;
use AppBundle\Entity\Timeslot;

class UserTest extends WebTestCase
{
    public function testProjectGetters()
    {
        // inicializace

        $user = new User();

        $jobA1 = new Job();
        $jobS1 = new Job();
        $jobX1 = new Job();

        $projectA1 = new Project(); // uživatel pracuje v tomto projektu jako zaměstnanec
        $projectS1 = new Project(); // uživatel pracuje v tomto projektu jako koordinátor
        $projectX1 = new Project(); // uživatel nemá s tímto projektem nic společného

        $jobA1 -> setProject($projectA1);
        $jobS1 -> setProject($projectS1);

        $user -> addAttending($jobA1);
        $user -> addSupervising($jobS1);

        // pozitivni scénáře

        $this -> assertTrue($user -> getAttendingProjects() -> contains($projectA1));
        $this -> assertTrue($user -> getSupervisingProjects() -> contains($projectS1));
        $this -> assertEquals(new ArrayCollection([$projectA1, $projectS1]), $user -> getAvailableProjects());

        // negativni scénáře

        $this -> assertFalse($user -> getAvailableProjects() -> contains($projectX1));
        $this -> assertFalse($user -> getAttendingProjects() -> contains($projectS1));
        $this -> assertFalse($user -> getSupervisingProjects() -> contains($projectA1));
    }

    public function testShiftByTimeslotGetter() {
        $user = new User();

        $timeslot1 = new Timeslot();
        $timeslot2 = new Timeslot();

        $shift1 = new Shift($user, $timeslot1);
        $shift2 = new Shift($user, $timeslot1);
        $shift3 = new Shift($user, $timeslot2);

        $user -> addShift($shift1);
        $user -> addShift($shift2);

        $this -> assertEquals(new ArrayCollection([$shift1, $shift2]), $user -> getShiftsByTimeslot($timeslot1));

    }
}
