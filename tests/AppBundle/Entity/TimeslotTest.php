<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Entity\User;
use AppBundle\Entity\Job;
use AppBundle\Entity\Project;
use AppBundle\Entity\Shift;
use AppBundle\Entity\Timeslot;

class TimeslotTest extends WebTestCase
{
    public function testOverlapping()
    {
        $data = [
            // [zacatek prvniho, konec prvniho, zacatek druheho, konec druheho, prekryvaji se]
            ['00:00', '08:00', '12:00', '18:00', false], // sloty jsou zcela odlisne
            ['00:00', '08:00', '07:00', '18:00', true],  // sloty se prekryvaji o jednu hodinu
            ['00:00', '08:00', '00:00', '08:00', true],  // sloty jsou stejne
            ['00:00', '08:00', '12:00', '18:00', false], // test meznich hodnot 1
            ['00:00', '08:00', '07:59', '23:59', true],  // test meznich hodnot 2
        ];

        foreach ($data as $item) {
            $timeslot1 = new Timeslot();
            $timeslot2 = new Timeslot();

            $timeslot1 -> setBeginTime($item[0]);
            $timeslot1 -> setEndTime($item[1]);

            $timeslot2 -> setBeginTime($item[2]);
            $timeslot2 -> setEndTime($item[3]);

            $this -> assertEquals($item[4], $timeslot1 -> overlapsWith($timeslot2));
            $this -> assertEquals($item[4], $timeslot2 -> overlapsWith($timeslot1));
        }
    }

    public function testCapacity() {
        $user = new User();
        $timeslot = new Timeslot();

        // test omezene kapacity

        $timeslot -> setCapacity(5);

        for ($i = 0; $i < 4; $i++) {
            $shift = new Shift($user, $timeslot);
            $timeslot -> addShift($shift); // nutne kvuli Doctrine getterum a setterum
            $shift -> setState(Shift::CONFIRMED);
        }

        $this -> assertFalse($timeslot -> isFull());

        // smeny ve stavech "zrusena" nebo "odmitnuta" nemaji na kapacitu vliv

        $shift1 = new Shift($user, $timeslot);
        $shift1 -> setState(Shift::CANCELLED);
        $timeslot -> addShift($shift1);

        $shift2 = new Shift($user, $timeslot);
        $shift2 -> setState(Shift::REJECTED);
        $timeslot -> addShift($shift2);

        $this -> assertFalse($timeslot -> isFull());

        // prekroceni kapacity

        $shift1 = new Shift($user, $timeslot);
        $shift1 -> setState(Shift::CONFIRMED);
        $timeslot -> addShift($shift1);

        $this -> assertTrue($timeslot -> isFull());

        // test neomezene kapacity

        $timeslot -> setCapacity(0);

        $this -> assertFalse($timeslot -> isFull());
    }
}
