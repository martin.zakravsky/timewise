<?php

namespace AppBundle\Twig;
use Doctrine\Common\Util\Debug as Debug;

class AppExtension extends \Twig_Extension {
    public function getFilters() {
        return [
            new \Twig_SimpleFilter('safedump', [$this, 'safedumpFilter']),
        ];
    }

    public function safedumpFilter($input) {
        return Debug::dump($input);
    }
}