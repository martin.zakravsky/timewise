<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use AppBundle\Entity\Project;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('username', TextType::class, ['label_format' => '%name%',])
            ->add('name', TextType::class, ['label_format' => '%name%',])
            ->add('surname', TextType::class, ['label_format' => '%name%',])
            ->add('dateOfBirth', BirthdayType::class, [
                'label_format' => '%name%',
                'widget' => 'single_text',
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Passwords must match',
                'required' => false,
                'options' => [
                    'label_format' => '%name%',
                    'required' => false,
                ],
                'first_options' => [
                    'label' => 'Password',
                ],
                'second_options' => [
                    'label' => 'Password repeated'
                ],
            ])
            ->add('email', EmailType::class, ['label_format' => '%name%',])
            ->add(
                'roles', ChoiceType::class, [
                    'choices' => [
                        'Administrator' => 'ROLE_ADMIN',
                        'Accountant' => 'ROLE_ACCOUNTANT'
                    ],
                    'expanded' => true,
                    'multiple' => true,
                    'label_format' => '%name%',
                ]
            )
            ->add('attending', EntityType::class, [
                    'class' => 'AppBundle:Job',
                    'expanded' => true,
                    'multiple' => true,
                    'choice_label' => 'name',
                    'required' => false,
                    'group_by' => function ($val, $key, $index) {
                        if ($val -> getProject()) {
                            return $val->getProject()->getName();
                        }
                        else {
                            return "(Not associated)";
                        }
                    }
                ]
            )

            ->add('supervising', EntityType::class, [
                    'class' => 'AppBundle:Job',
                    'expanded' => true,
                    'multiple' => true,
                    'choice_label' => 'name',
                    'required' => false,
                    'group_by' => function ($val, $key, $index) {
                        if ($val -> getProject()) {
                            return $val->getProject()->getName();
                        }
                        else {
                            return "!!!";
                        }
                    }
                ]
            )
            ->add('submit', SubmitType::class, [
                'label_format' => '%name%',
                'attr' => [
                    'class' =>  'basic_button'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}