<?php

namespace AppBundle\Form;

use AppBundle\Entity\Timeslot;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use AppBundle\Entity\Job;
use Doctrine\ORM\EntityRepository;


class TimeslotType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $current_user = $options["current_user"];

        $builder
            ->add('date', DateType::class, [
                'label_format' => '%name%',
                'disabled'=> true,
                'widget' => 'single_text',

            ])
            ->add('job', EntityType::class, [
                'label_format' => '%name%',
                'class' => Job::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) use ($current_user) {

                    $qb = $er -> createQueryBuilder('j');
                    return $qb
                        -> where('j.project = :p')
                        -> andWhere('j IN(:sj)')
                        -> setParameter(':p', $current_user -> getPreferredProject() -> getId())
                        -> setParameter(':sj', $current_user -> getSupervising() -> toArray())
                        ;
                }
            ])
            ->add('beginTime', TimeType::class, [
                'label_format' => '%name%',
                'widget' => 'single_text',
            ])
            ->add('endTime', TimeType::class, [
                'label_format' => '%name%',
                'widget' => 'single_text',
            ])
            ->add('open', CheckboxType::class, [
                'label_format' => '%name%',
                'required' => false,
            ])
            ->add('capacity', NumberType::class, [
                'label_format' => '%name%',
                'required' => false,
            ])
            ->add('comment', TextareaType::class, [
                'label_format' => '%name%',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label_format' => '%name%',
                'attr' => [
                    'class' => 'basic_button'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Timeslot::class,
            'current_user' => null
        ));
    }
}