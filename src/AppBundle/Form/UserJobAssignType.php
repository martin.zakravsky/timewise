<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use AppBundle\Entity\Project;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserJobAssignType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $current_user = $options['current_user'];

        $builder
            ->add('attending', EntityType::class, [
                    'class' => 'AppBundle:Job',
                    'expanded' => true,
                    'multiple' => true,
                    'choice_label' => 'name',
                    'query_builder' => function (EntityRepository $er) use ($current_user) {
                        return $er -> createQueryBuilder('j')
                            -> leftJoin('j.project', 'p')
                            -> where('p.id = :id')
                            -> setParameter(':id', $current_user -> getPreferredProject() -> getID())
                            ;
                    }
                ]
            )
            ->add('submit', SubmitType::class, [
                'label_format' => '%name%',
                'attr' => [
                    'class' =>  'basic_button'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'current_user' => null,
        ));
    }
}