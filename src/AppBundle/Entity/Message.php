<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="messages")
 */
class Message
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var date $datetimeOfOrigin
     *
     * @ORM\Column(name="datetime_of_origin", type="datetime", nullable=false)
     */
    private $datetimeOfOrigin;

    /**
     * @var text $content
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateOfOrigin
     *
     * @param \DateTime $dateOfOrigin
     *
     * @return Message
     */
    public function setDateOfOrigin($dateOfOrigin)
    {
        $this->dateOfOrigin = $dateOfOrigin;

        return $this;
    }

    /**
     * Get dateOfOrigin
     *
     * @return \DateTime
     */
    public function getDateOfOrigin()
    {
        return $this->dateOfOrigin;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Message
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set datetimeOfOrigin
     *
     * @param \DateTime $datetimeOfOrigin
     *
     * @return Message
     */
    public function setDatetimeOfOrigin($datetimeOfOrigin)
    {
        $this->datetimeOfOrigin = $datetimeOfOrigin;

        return $this;
    }

    /**
     * Get datetimeOfOrigin
     *
     * @return \DateTime
     */
    public function getDatetimeOfOrigin()
    {
        return $this->datetimeOfOrigin;
    }

    /**
     * Set sender
     *
     * @param \AppBundle\Entity\User $sender
     *
     * @return Message
     */
    public function setSender(\AppBundle\Entity\User $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \AppBundle\Entity\User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set receiver
     *
     * @param \AppBundle\Entity\User $receiver
     *
     * @return Message
     */
    public function setReceiver(\AppBundle\Entity\User $receiver = null)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return \AppBundle\Entity\User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Message
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Message constructor.
     */
    public function __construct() {
        $this -> datetimeOfOrigin = new \DateTime();
    }
}
