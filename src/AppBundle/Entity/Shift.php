<?php

namespace AppBundle\Entity;
use AppBundle\Exception\IllegalShiftSwitchException;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Shift;
use Doctrine\ORM\EntityRepository;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShiftRepository")
 * @ORM\Table(name="shifts")
 */
class Shift
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $state
     *
     * @ORM\Column(name="state", type="smallint")
     */
    private $state;

    /**
     * @var string $comment
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="Timeslot", inversedBy="shifts")
     * @ORM\JoinColumn(name="timeslot_id", referencedColumnName="id")
     */
    private $timeslot;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="shifts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Version
     */
    private $version;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    const UNCONFIRMED = 0;
    const CONFIRMED = 1;
    const REJECTED = 2;
    const REQUIRES_CANCELLATION = 3;
    const CANCELLED = 4;
    const FINISHED = 5;
    const MISSED = 6;

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Shift
     */
    public function setState($requiredState)
    {
        $this -> state = $requiredState;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Shift
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set timeslot
     *
     * @param \AppBundle\Entity\Timeslot $timeslot
     *
     * @return Shift
     */
    public function setTimeslot(\AppBundle\Entity\Timeslot $timeslot = null)
    {
        $this->timeslot = $timeslot;

        return $this;
    }

    /**
     * Get timeslot
     *
     * @return \AppBundle\Entity\Timeslot
     */
    public function getTimeslot()
    {
        return $this->timeslot;
    }


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Shift
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get timeslot's job
     *
     * @return Job
     */

    public function getJob() {
        return $this -> timeslot -> getJob();
    }

    /**
     * See if shift is in a final state
     *
     * @return boolean
     */
    public function isInFinalState() {
        return !in_array($this -> getState(), [
            Shift::UNCONFIRMED,
            Shift::CONFIRMED,
            Shift::REQUIRES_CANCELLATION,
            Shift::FINISHED,
            Shift::MISSED,
        ]);
    }

    /**
     * Shift constructor.
     * @param Timeslot $timeslot
     * @param User $user
     */
    public function __construct(User $user, Timeslot $timeslot) {
        $this -> timeslot = $timeslot;
        $this -> user = $user;

        // supervisors' shifts will be automatically approved
        if ($user -> getSupervising() -> contains($timeslot -> getJob())) {
            $this->state = Shift::CONFIRMED;
        }
        else {
            $this -> state = Shift::UNCONFIRMED;
        }
    }


    /**
     * Set version
     *
     * @param integer $version
     *
     * @return Shift
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }
}
