<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TimeslotRepository")
 * @ORM\Table(name="timeslots", uniqueConstraints={@ORM\UniqueConstraint(name="jdbe", columns={"job_id", "date", "begin_time", "end_time"})}) TODO
 */
class Timeslot
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $date
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var \DateTime $beginTime
     *
     * @ORM\Column(name="begin_time", type="time", nullable=false)
     */
    private $beginTime;

    /**
     * @var \DateTime $endTime
     *
     * @ORM\Column(name="end_time", type="time", nullable=false)
     */
    private $endTime;

    /**
     * @var int $capacity
     *
     * @ORM\Column(name="capacity", type="integer", nullable=true)
     */
    private $capacity;

    /**
     * @var boolean $open
     *
     * @ORM\Column(name="open", type="boolean", nullable=true)
     */
    private $open;

    /**
     * @var string $comment
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


    /**
     * @ORM\ManyToOne(targetEntity="Job", inversedBy="timeslots")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id")
     */
    private $job;

    /**
     * @ORM\OneToMany(targetEntity="Shift", mappedBy="timeslot")
     */
    private $shifts;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Version
     */
    private $version;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Timeslot
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set beginTime
     *
     * @param \DateTime $beginTime
     *
     * @return Timeslot
     */
    public function setBeginTime($beginTime)
    {
        $this->beginTime = $beginTime;

        return $this;
    }

    /**
     * Get beginTime
     *
     * @return \DateTime
     */
    public function getBeginTime()
    {
        return $this->beginTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Timeslot
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set capacity
     *
     * @param integer $capacity
     *
     * @return Timeslot
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get capacity
     *
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Timeslot
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->shifts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set job
     *
     * @param \AppBundle\Entity\Job $job
     *
     * @return Timeslot
     */
    public function setJob(\AppBundle\Entity\Job $job = null)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return \AppBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Add shift
     *
     * @param \AppBundle\Entity\Shift $shift
     *
     * @return Timeslot
     */
    public function addShift(\AppBundle\Entity\Shift $shift)
    {
        $this->shifts[] = $shift;

        return $this;
    }

    /**
     * Remove shift
     *
     * @param \AppBundle\Entity\Shift $shift
     */
    public function removeShift(\AppBundle\Entity\Shift $shift)
    {
        $this->shifts->removeElement($shift);
    }

    /**
     * Get shifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShifts()
    {
        return $this->shifts;
    }

    /**
     * Get job's project
     *
     * @return Project
     */
    public function getProject() {
        return $this -> job -> getProject();
    }

    /**
     * Set open
     *
     * @param boolean $open
     *
     * @return Timeslot
     */
    public function setOpen($open)
    {
        $this->open = $open;

        return $this;
    }

    /**
     * Get open
     *
     * @return boolean
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this -> beginTime >= $this -> endTime) {
            $context -> buildViolation ('The end time of a timeslot must be after its beginning time')
                -> atPath('endTime')
                -> addViolation();
        }
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return Timeslot
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    public function overlapsWith(Timeslot $b) {
        if ($this -> getDate() != $b -> getDate()) return false;

        $a1 = $this -> getBeginTime();
        $a2 = $this -> getEndTime();

        $b1 = $b -> getBeginTime();
        $b2 = $b -> getEndTime();

        if (max($a1, $b1) <= min($a2, $b2)) {
            return true;
        }
        else {
            return false;
        }
    }

    public function getTakenSpots() {
        return $this -> getShifts() -> filter(function($item) {
            return !in_array($item -> getState(), [
                Shift::CANCELLED,
                Shift::REJECTED,
            ]);
        }) -> count();
    }

    public function isFull() {
        if ($this -> capacity == 0) return false;
        return $this -> getTakenSpots() >= $this -> capacity;
    }

    public function getHours() {
        return $this -> getEndTime() - $this -> getBeginTime();
    }

}
