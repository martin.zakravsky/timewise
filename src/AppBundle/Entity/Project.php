<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="projects", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})})
 */
class Project
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="allows_overlapping_shifts", type="boolean", nullable=true)
     */
    private $allowsOverlappingShifts;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;



    /**
     *
     * @ORM\OneToMany(targetEntity="Job", mappedBy="project", cascade={"persist"})
     */
    private $jobs;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Version
     */
    private $version;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set allowsOverlappingShifts
     *
     * @param boolean $allowsOverlappingShifts
     *
     * @return Project
     */
    public function setAllowsOverlappingShifts($allowsOverlappingShifts)
    {
        $this->allowsOverlappingShifts = $allowsOverlappingShifts;

        return $this;
    }

    /**
     * Get allowsOverlappingShifts
     *
     * @return bool
     */
    public function getAllowsOverlappingShifts()
    {
        return $this->allowsOverlappingShifts;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Project
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Project
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add job
     *
     * @param \AppBundle\Entity\Job $job
     *
     * @return Project
     */
    public function addJob(\AppBundle\Entity\Job $job)
    {
        $this->jobs[] = $job;
        $job -> setProject($this);

        return $this;
    }

    /**
     * Remove job
     *
     * @param \AppBundle\Entity\Job $job
     */
    public function removeJob(\AppBundle\Entity\Job $job)
    {
        $this->jobs->removeElement($job);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Remove member
     *
     * @param \AppBundle\Entity\User $member
     */
    public function removeEmployee(\AppBundle\Entity\User $member)
    {
        foreach ($this -> jobs as $job) {
            $job -> removeEmployee($member);
        }
    }

    /**
     * Get members
     *
     * @return ArrayCollection
     */
    public function getEmployees()
    {
        $res = new ArrayCollection();

        foreach ($this -> getJobs() as $job) {
            foreach ($job -> getEmployees() as $employee) {
                if (!($res -> contains($employee))) $res -> add($employee);
            }
        }

        return $res;
    }


    /**
     * Remove supervisor
     *
     * @param \AppBundle\Entity\User $member
     */
    public function removeSupervisor(\AppBundle\Entity\User $user)
    {
        foreach ($this -> jobs as $job) {
            $job -> removeEmployee($user);
        }
    }

    /**
     * Get supervisors
     *
     * @return ArrayCollection
     */
    public function getSupervisors()
    {
        $res = new ArrayCollection();

        foreach ($this -> jobs as $job) {
            foreach ($job -> getSupervisors as $supervisor) {
                if (!($res -> contains($supervisor))) $res -> add($supervisor);
            }
        }

        return $res;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return Project
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    public function __construct() {
        $this -> jobs = new ArrayCollection();
    }
}
