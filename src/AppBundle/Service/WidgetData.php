<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 2.5.17
 * Time: 14:37
 */

namespace AppBundle\Service;


use Doctrine\Common\Collections\ArrayCollection;

class WidgetData {
    protected $data;
    protected $hiddenData;

    public function __construct() {
        $this -> data = new ArrayCollection();
        $this -> hiddenData = new ArrayCollection();
    }

    public function setData(ArrayCollection $data) {
        $this -> data = $data;
    }

    public function getData() {
        return $this -> data;
    }

    public function setHiddenData(ArrayCollection $data) {
        $this -> hiddenData = $data;
    }

    public function getHiddenData() {
        return $this -> hiddenData;
    }

    public function setDataVal($key, $val) {
        $this -> data -> set($key, $val);
    }

    public function setHiddenDataVal($key, $val) {
        $this -> hiddenData -> set($key, $val);
    }
}