<?php

namespace AppBundle\Service;

use \Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Doctrine\ORM\PersistentCollection;

class PaginationWidget
{

    /**
     * @var ArrayCollection
     */
    protected $items;

    /**
     * @var integer
     */
    protected $currentPage = 1;

    /**
     * @var integer
     */
    protected $itemsPerPage = 10;

    /**
     * @return integer
     */
    public function getNumOfPages() {
        return (integer)ceil($this -> items -> count() / $this -> itemsPerPage);
    }

    /**
     * @return array
     */
    public function getCurrentPageData() {
        return $this -> items -> slice(($this -> currentPage - 1) * $this -> itemsPerPage, $this -> itemsPerPage);
    }

    public function nextPage() {
        if (isAtLastPage()) return;
        $this -> currentPage++;
    }

    public function previousPage() {
        if (isAtFirstPage()) return;
        $this -> currentPage--;
    }

    /**
     * @return bool
     */
    public function isAtLastPage() {
        return $this -> currentPage == $this -> getNumOfPages();
    }

    /**
     * @return bool
     */
    public function isAtFirstPage() {
        return $this -> currentPage == 1;
    }

    /**
     * @return integer
     */
    public function getLastPage() {
        return $this -> getNumOfPages();
    }

    /**
     * PaginationWidget constructor.
     */
    public function __construct() {
        $this -> items = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @param ArrayCollection $item
     */
    public function setItems($items)
    {
        if (is_array($items)) {
            $this->items = new ArrayCollection($items);
        }
        else if ($items instanceOf ArrayCollection) {
            $this->items = $items;
        }
        else if ($items instanceOf PersistentCollection) {
            $this -> items = new ArrayCollection();
            foreach ($items -> toArray() as $item) {
                $this -> items -> add($item);
            }
        }
        else throw new \InvalidArgumentException(get_class($items));
    }

    /**
     * @return int
     */
    public function getCurrentPage() {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage($currentPage) {
        if (!is_numeric($currentPage) || $currentPage < 1 || $currentPage > $this -> getNumOfPages()) $currentPage = 1;
        $this->currentPage = $currentPage;
    }

    /**
     * @return int
     */
    public function getItemsPerPage() {
        return $this->itemsPerPage;
    }

    /**
     * @param int $itemsPerPage
     */
    public function setItemsPerPage($itemsPerPage) {
        if ($itemsPerPage < 1) throw new OutOfRangeException ("There must be at least one item per page");
        $this->itemsPerPage = $itemsPerPage;
    }

    public function validateOrReset($page) {
        if (!is_numeric($page) || $page < 1 || $page > $this -> getNumOfPages()) return 1;
        else return $page;
    }

}