$(document).ready(function() {
    $('.add_field').click(function() {
        var new_id = $(this).data("new-id");
        var val1 = $(this).data("val1");
        var val2 = $(this).data("val2");
        var target_parent = $(this).data("target-parent");

        var new_field = document.createElement("input");
        var new_form_group = document.createElement("div");
        var row_wrapper = document.createElement("div");
        var field_wrapper = document.createElement("div");

        new_form_group.setAttribute("class", "project_jobs_"+new_id);

        new_field.setAttribute("type", "text");
        new_field.setAttribute("required", "required");
        new_field.setAttribute("class", "form-control");
        new_field.setAttribute("name", val1+"["+val2+"]["+new_id+"][name]");
        new_field.setAttribute("id", val1+"_"+val2+"_"+new_id+"_name");

        row_wrapper.setAttribute("class", "row_wrapper");
        field_wrapper.setAttribute("class", "field_wrapper");

        field_wrapper.append(new_field);

        row_wrapper.append(field_wrapper);

        new_form_group.append(row_wrapper);

        $('#'+target_parent).append(new_form_group);
        $(this).data("new-id", new_id+1);
    });
});