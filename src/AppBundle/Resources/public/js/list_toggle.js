$(document).ready(function() {
    $('.list_toggle').click(function() {
        var target = $('#' + $(this).data('target'));
        target.toggle();
        $(this).toggleClass('selected');
    });
});

