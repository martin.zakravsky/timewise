$(document).ready(function() {
    $('.dynamic_requester').each(function(index) {
        transformButton($(this), $(this).attr('data-req-type'), $('#' + $(this).attr('data-other-button')), true);
    });

    $('.dynamic_requester').click(function() {
        // Declare vars

        var button = $(this);
        var button_other = $('#' + $(this).attr('data-other-button'));
        var req_type = $(this).attr('data-req-type');
        var result_req_type;
        var result_class;
        var result_label;

        // If this is just a placeholder blocking button, abort

        if (req_type == '') return;

        // css...

        var processing_class = 'dynamic_request_processing';
        var error_class = 'dynamic_request_error';


        // Set up the processing look

        $(this).addClass(processing_class);
        $(this).html('...');

        // Send the request

        if (req_type == 'supervisor_confirm_cancel_request') req_type = 'supervisor_cancel';
        if (req_type == 'supervisor_reject_cancel_request') {
            alert();
            req_type = 'supervisor_confirm';
        }

        $.ajax({
            url: button.attr("data-"+req_type).replace('@CSRF', $('#ajax_token').attr('value'))
        }).done(function(data){
            callbackSuccessfulAjax(button, req_type, button_other, data);
        })
        .fail(function(data) {
            button.addClass(error_class);
            button.attr('data-req-type', '');
            translate(button, '!!!');
            translate(flashMessage('error', ''), data.responseText);
            console.log(data);
        });

    });

    function callbackSuccessfulAjax(button, req_type, button_other, data) {
        $('#ajax_token').attr('value', data);

        // Decide, how to continue after the process is done

        if (req_type == 'employee_sign_up') {
            transformButton(button, 'employee_cancel', button_other, false);
            button.attr('data-csrf-token', data.responseText);
        }
        else if (req_type == 'employee_cancel' || req_type == 'employee_cancel_by_timeslot' || req_type == 'employee_cancel_as_supervisor') {
            transformButton(button, 'employee_cancelled', button_other, false);
        }
        else if (req_type == 'employee_request_cancel') {
            transformButton(button, 'employee_requested_cancel', button_other, false);
        }
        else if (req_type == 'supervisor_confirm') {
            transformButton(button, 'supervisor_confirmed', button_other, false);
        }
        else if (req_type == 'supervisor_reject') {
            transformButton(button, 'supervisor_rejected', button_other, false);
        }
        else if (req_type == 'supervisor_cancel') {
            transformButton(button, 'supervisor_cancelled', button_other, false);
        }
        else if (req_type == 'supervisor_mark_as_missed') {
            transformButton(button, 'supervisor_mark_as_finished', button_other, false);
        }
        else if (req_type == 'supervisor_mark_as_finished') {
            transformButton(button, 'supervisor_mark_as_missed', button_other, false);
        }
    }

    function transformButton(button, req_type, button_other, first) {
        button.removeClass(); // remove all classes
        button.addClass('dynamic_requester');
        button.addClass('shift_button');

        if (!first) {
            if (req_type == 'employee_cancel') req_type = 'employee_cancel_by_timeslot';
            button_other.hide();
            button.attr('data-req-type', req_type);
        }

        if (req_type == 'employee_sign_up') {
            button.addClass('shift_button_positive');
            translate(button, 'Sign up');
        }
        else if (req_type == 'employee_cancel' || req_type == 'employee_cancel_by_timeslot' || req_type == 'employee_cancel_as_supervisor') {
            button.addClass('shift_button_negative');
            translate(button, 'Cancel');
        }
        else if (req_type == 'employee_request_cancel') {
            button.addClass('shift_button_negative');
            translate(button, 'Request cancel');
        }
        else if (req_type == 'supervisor_confirm') {
            button.addClass('shift_button_positive');
            translate(button, 'Confirm');
        }
        else if (req_type == 'supervisor_reject') {
            button.addClass('shift_button_negative');
            translate(button, 'Reject');
        }
        else if (req_type == 'supervisor_cancel') {
            button.addClass('shift_button_negative');
            translate(button, 'Cancel');
        }
        else if (req_type == 'supervisor_confirm_cancel_request') {
            button.addClass('shift_button_positive');
            translate(button, 'Cancel on request');
        }
        else if (req_type == 'supervisor_reject_cancel_request') {
            button.addClass('shift_button_negative');
            translate(button, 'Keep');
        }
        else if (req_type == 'supervisor_cancelled' || req_type == 'employee_cancelled' ) {
            button.addClass('shift_button_disabled');
            translate(button, 'Cancelled');
        }
        else if (req_type == 'supervisor_rejected' ) {
            button.addClass('shift_button_disabled');
            translate(button, 'Rejected');
        }
        else if (req_type == 'employee_requested_cancel') {
            button.addClass('shift_button_disabled');
            translate(button, 'Requested cancel');
        }
        else if (req_type == 'supervisor_confirmed') {
            button.addClass('shift_button_complete');
            translate(button, 'Confirmed');
        }
        else if (req_type == 'supervisor_marked_as_missed') {
            button.addClass('shift_button_missed');
            translate(button, 'Missed');
        }
        else if (req_type == 'supervisor_marked_as_finished') {
            button.addClass('shift_button_complete');
            translate(button, 'Finished');
        }
        else if (req_type == 'supervisor_mark_as_missed') {
            button.addClass('shift_button_negative');
            translate(button, 'Mark as missed');
        }
        else if (req_type == 'supervisor_mark_as_finished') {
            button.addClass('shift_button_positive');
            translate(button, 'Mark as finished');
        }

        if (button.attr('data-force-value') != null) {
            translate(button, button.attr('data-force-value'));
            button.attr('data-force-value', null);
        }
    }
});

