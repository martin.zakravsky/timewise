$(document).ready(function() {
    $('.delete_confirmation_button').click(function() {
        var id = $(this).data("id");
        $('div[data-id-confirm-popup=' + id + ']').toggle();
    });

    $('.delete_cancellation_button').click(function() {
        $(this).parent().parent().hide();
    });
});