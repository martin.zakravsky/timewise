function flashMessage (type, content = '') {
    if (type != 'notice' && type != 'error' && type != 'warning') {
        return;
    }

    type = 'flash_' + type;

    var message = $('<div class=\"' + type + '\">' + content + '</div>');

    $('.message_bag').append(message);

    return message;
}