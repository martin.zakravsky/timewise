function translate(element, val) {
    var data = {
        original: val,
        translated: null
    };

    if (localStorage.getItem(data.original) == null) {
        $.post("/translate/", data, function (response) {

            // pro nizsi zatez serveru si prohlizec pamatuje jiz prelozene vyrazy
            localStorage.setItem(data.original, response.translated);
            element.html(response.translated);
        });
    }
    else {
        element.html(localStorage.getItem(data.original));
    }
}

