<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;

class AdminController extends Controller
{
    /**
     * @Route("/admin/", name="admin")
     */
    public function adminAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', NULL, 'error.accessDenied');

        return $this->render('admin/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/admin/user_overview/")
     * @Route("/admin/user_overview/{page}", name="admin_user_overview")
     */
    public function userOverviewAction($page = 1) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', NULL, 'error.accessDenied');

        $userRepo = $this->getDoctrine()->getRepository('AppBundle:User');
        $allUsers = $userRepo->findBy([],['username' => 'ASC']);

        $paginationWidget = $this->get('pagination_widget');
        $paginationWidget -> setItems($allUsers);
        $page = $paginationWidget -> validateOrReset($page);
        $paginationWidget -> setCurrentPage($page);

        $pagedUsers = $paginationWidget -> getCurrentPageData();

        $tokens = new ArrayCollection();

        $tokenProvider = $this->get('security.csrf.token_manager');

        foreach ($pagedUsers as $user) {
            $tokens -> set($user -> getId(), $tokenProvider -> refreshToken('delete_'.$user -> getId()));
        }

        return $this->render('admin/user_overview.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'users' => $pagedUsers,
            'return_path' => 'admin',
            'pw' => $paginationWidget,
            'tokens' => $tokens,
        ]);
    }

    /**
     * @Route("/admin/project_overview/")
     * @Route("/admin/project_overview/{page}", name="admin_project_overview")
     */
    public function projectOverviewAction($page = 1) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', NULL, 'error.accessDenied');

        $repo = $this->getDoctrine()->getRepository('AppBundle:Project');
        $allProjects = $repo -> findBy([], ['name' => 'ASC']);
        $paginationWidget = $this->get('pagination_widget');
        $paginationWidget -> setItems($allProjects);
        $paginationWidget -> setCurrentPage($page);
        $pagedProjects = $paginationWidget -> getCurrentPageData();

        $tokens = new ArrayCollection();

        $tokenProvider = $this->get('security.csrf.token_manager');

        foreach ($pagedProjects as $project) {
            $tokens -> set($project -> getId(), $tokenProvider -> refreshToken('delete_'.$project -> getId()));
        }

        return $this->render('admin/project_overview.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'projects' => $pagedProjects,
            'return_path' => 'admin',
            'pw' => $paginationWidget,
            'tokens' => $tokens,
        ]);
    }
}
