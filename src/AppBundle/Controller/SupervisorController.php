<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 22.3.17
 * Time: 16:45
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Timeslot;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Exception\NotImplementedException;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Form\UserJobAssignType;
use AppBundle\Form\TimeslotType;
use AppBundle\Entity\User;

class SupervisorController extends Controller {
    /**
     * @Route("/supervisor/", name="supervisor")
     */
    public function supervisorAction()
    {
        $this->checkPermissions();

        return $this->render('supervisor/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }



    /**
     * @Route("/supervisor/user_overview/", name="supervisor_user_overview")
     * @Route("/supervisor/user_overview/{page}")
     */
    public function userOverviewAction($page = 1) {
        if (!is_integer($page) || $page < 1) $page = 1;

        $this -> checkPermissions();

        $user = $this -> getUser();
        $preferredProject = $this -> getUser() -> getPreferredProject();

        // we want to only see users that will have the jobs of this supervisor - that's why we need to get his jobs


        /*
        // we want to group the users by jobs
        $usersByJobs = array();
        foreach ($myJobs as $job) {
            $usersByJobs[$job -> getName()] = $relevantUsers->filter(function ($user) use ($job) {
                $usersJobs = $user -> getAttending();
                return $usersJobs -> contains ($job);
            }) -> toArray();
        }
        */

       $usersByJobs = $this -> getDoctrine() -> getRepository("AppBundle:User") -> findEmployeesByMyJobs($user, $preferredProject);


        /*
        $paginationWidget = $this->get('pagination_widget');
        $paginationWidget -> setItems($relevantUsers);
        $paginationWidget -> setCurrentPage($page);
        $pagedUsers = $paginationWidget -> getCurrentPageData();
        */

        return $this->render('supervisor/user_overview.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'usersByJobs' => $usersByJobs,
            'return_path' => 'supervisor',
        ]);
    }

    /**
     * @Route("/supervisor/plan/{items}/{view}")
     * @Route("/supervisor/plan/{items}/{view}/{year}/{month}/{day}", name="supervisor_plan")
     */
    public function planAction(Request $request, $items='timeslot', $view='calendar', $year = null, $month = null, $day = null) {
        $this -> checkPermissions();

        // Validate fields

        if (empty($day)) $day = date("d");
        if (empty($month)) $month = date("m");
        if (empty($year)) $year = date("Y");
        if (!checkdate($month, $day, $year)) throw new InvalidInputException("Invalid date");
        $date = new \DateTime($year."-".$month."-".$day);

        $user = $this -> getUser();
        $preferredProject = $user -> getPreferredProject();

        // Load productivity diagram

        $wddo = $this -> get('widget_data');
        $wddo -> setData($this -> getDoctrine() -> getRepository("AppBundle:Timeslot") -> countOccupied($date, $preferredProject, $user));

        if (
            !in_array($items, ['timeslot', 'shift']) ||
            !in_array($view, ['calendar', 'daily'])
        ) throw new InvalidInputException("Invalid overview argument");



        $data = null;
        $start_day = clone $date;
        $end_day = clone $date;
        $render_path = null;
        $return_path = 'supervisor';
        $tokenProvider = $this->get('security.csrf.token_manager');
        $token = $tokenProvider -> refreshToken('ajax');
        $tokens = new ArrayCollection();

        if ($items == 'timeslot') {

            if ($view == 'calendar') {
                $start_day -> modify('first day of this month');
                $end_day -> modify('last day of this month');
            }
            elseif ($view == 'daily') {
                // nothing, keeping for readability
            }

            $data = $this -> getDoctrine() -> getRepository('AppBundle:Timeslot') -> findByUser($user, $start_day, $end_day, FALSE, $preferredProject);

            foreach ($data as $timeslot) {
                $tokens -> set($timeslot -> getId(), $tokenProvider -> refreshToken('delete_'.$timeslot->getId()));
            }


        }
        else {
            if ($view == 'calendar') {
                $start_day -> modify('first day of this month');
                $end_day -> modify('last day of this month');
            }
            elseif ($view == 'daily') {
                // nothing, keeping for readability
            }

            $data = $this -> getDoctrine() -> getRepository('AppBundle:Shift') -> findByUser($user, $start_day, $end_day, FALSE, $preferredProject);
        }

        return $this->render('supervisor/'.$items.'_overview/'.$view.'_view.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'month' => $month,
            'year' => $year,
            'day' => $day,
            'shifts' => $data,
            'timeslots' => $data,
            'items' => $items,
            'view' => $view,
            'return_path' => $return_path,
            'selected_date' => $date,
            'wddo' => $wddo,
            'ajax_token' => $token,
            'tokens' => $tokens,
        ]);

    }

    /**
     * @Route("/supervisor/todo_list/", name="supervisor_todo_list")
     */
    public function todoList(Request $request) {
        $this -> checkPermissions();
        $user = $this -> getUser();
        $shifts = $this -> getDoctrine() -> getRepository("AppBundle:Shift") -> findByUser($user, null, null, false, $user -> getPreferredProject());

        $tokenProvider = $this->get('security.csrf.token_manager');
        $token = $tokenProvider -> refreshToken('ajax');

        return $this -> render("supervisor/todo_list.html.twig", [
                'shifts' => $shifts,
                'return_path' => 'supervisor',
                'ajax_token' => $token,
            ]);
    }

    private function checkPermissions() {
        $current_project = $this -> getUser() -> getPreferredProject();
        $current_user = $this -> getUser();
        if (!($current_user -> getSupervisingProjects() -> contains($current_project))) {
            throw $this -> createNotFoundException();
        }
    }
}
