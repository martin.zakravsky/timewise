<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 3.4.17
 * Time: 15:03
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Timeslot;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Exception\NotImplementedException;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Form\UserJobAssignType;
use AppBundle\Form\TimeslotType;
use AppBundle\Exception\InvalidInputException;


class TimeslotController extends Controller
{
    /**
     * @Route("/supervisor/create_timeslot/{year}/{month}/{day}", name="supervisor_create_timeslot")
     */
    public function createTimeslot(Request $request, $year, $month, $day) {
        $this -> checkPermissions();

        if (!checkdate($month, $day, $year)) throw new \InvalidArgumentException();

        $selected_date = $year."-".$month."-".$day;

        return $this -> modifyTimeslotAction($request, null, $selected_date);

    }

    /**
     * @Route("/supervisor/modify_timeslot/{id}", name="supervisor_modify_timeslot")
     */
    public function modifyTimeslotAction (Request $request, $id = null, $selected_date = null) {
        $this -> checkPermissions();
        if (empty($id)) {
            $timeslot = new Timeslot();
            $timeslot -> setDate(new \DateTime($selected_date));
        }
        else {
            $timeslot = $this -> getDoctrine() -> getRepository("AppBundle:Timeslot") ->findOneById($id);
            if ($timeslot == null) throw $this -> createNotFoundException();
        }

        $timeslots = [];

        $selected_date = $timeslot -> getDate();

        $form = $this -> createForm(TimeslotType::class, $timeslot, [
            'current_user' => $this -> getUser()
        ]);
        $form -> handleRequest($request);

        try {
            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($timeslot);
                    $em->flush();
                    $this->addFlash("notice", "Timeslot saved!");

                    return $this->redirectToRoute("supervisor_plan", [
                        'items' => 'timeslot',
                        'view' => 'daily',
                        'year' => $selected_date -> format("Y"),
                        'month' => $selected_date -> format("m"),
                        'day' => $selected_date -> format('d'),
                    ]);
                }
            }
        }
        catch (UniqueConstraintViolationException $ex) {
            $this -> addFlash("error", "Timeslot with that interval already exists for this job");
            return $this->redirectToRoute("supervisor_timeslot_overview");
        }

        return $this->render('supervisor/modify_timeslot.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'selected_date' => $selected_date,
            'timeslots' => $timeslots,
            'form' => $form -> createView(),
            'return_path' => 'supervisor_plan',
        ]);
    }

    /**
     * @Route("/supervisor/delete_timeslot/{id}", name="supervisor_delete_timeslot")
     */
    public function deleteTimeslotAction(Request $request, $id) {
        $this -> checkPermissions();
        if (!is_numeric($id) || $id < 1) throw new InvalidInputException("Invalid ID");


        $em = $this -> getDoctrine() -> getManager();

        $timeslotRepo = $this -> getDoctrine() -> getRepository("AppBundle:Timeslot");

        $timeslot = $timeslotRepo -> findOneById($id);

        if ($timeslot == null || !($this -> isCsrfTokenValid('delete_'.$timeslot -> getId(), $request -> get('_csrf_token')))) {
            throw new InvalidInputException("CSRF protection violated");
        }

        try {
            $em -> remove($timeslot);
            $em -> flush();
            $this -> addFlash('notice', 'Timeslot deleted!');
        }
        catch(DBALException $ex) {
            $this -> addFlash('error', 'You cannot delete a timeslot with shifts');
        }
        catch(\Exception $ex) {
            $this -> addFlash('error', 'Unknown error occurred');
        }

        return $this -> redirect($request -> headers -> get('referer'));
    }

    private function checkPermissions() {
        $current_project = $this -> getUser() -> getPreferredProject();
        $current_user = $this -> getUser();
        if (!($current_user -> getSupervisingProjects() -> contains($current_project))) {
            throw $this -> createNotFoundException();
        }
    }
}