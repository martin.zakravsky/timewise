<?php

namespace AppBundle\Controller;

use AppBundle\Exception\ConcurrencyException;
use AppBundle\Exception\InvalidInputException;
use AppBundle\Exception\InvalidMethodUsedException;
use AppBundle\Form\UserWageType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\Tools\Export\ExportException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Exception\UserNotFoundException;
use AppBundle\Form\UserSelfType;
use AppBundle\Form\UserJobAssignType;

class UserController extends Controller
{
    /**
     * @Route("/admin/create_user", name="admin_create_user")
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', NULL, 'error.accessDenied');

        return $this-> redirectToRoute("admin_modify_user", ['userID' => 0]);
    }

    /**
     * @Route("/admin/delete_user/{id}", name="admin_delete_user")
     */
    public function deleteAction(Request $request, $id) {
        $this -> denyAccessUnlessGranted('ROLE_ADMIN', NULL, 'You don\'t have the necessary permissions to access this function');
        if (!is_numeric($id) || $id < 1) throw new InvalidInputException("Invalid ID");


        $em = $this -> getDoctrine() -> getManager();

        $userRepo = $this -> getDoctrine() -> getRepository("AppBundle:User");

        $user = $userRepo -> findOneById($id);

        if ($user == null || !($this -> isCsrfTokenValid('delete_'.$user -> getId(), $request -> get('_csrf_token')))) {
            throw new InvalidInputException("CSRF protection violated");
        }

        try {
            $em -> remove($user);
            $em -> flush();
        }
        catch(\Exception $ex) {
            $this -> addFlash('error', 'Unknown error occurred');
            $this -> addFlash('error', $ex ->getMessage());
        }

        return $this -> redirect($request -> headers -> get('referer'));
    }
    /**
     * @Route("/admin/modify_user/{userID}", name="admin_modify_user")
     */
    public function modifyAction(Request $request, $userID) {
        $this -> denyAccessUnlessGranted('ROLE_ADMIN', NULL, 'error.accessDenied');
        if ($userID > 0) {
            $userRepo = $this->getDoctrine()->getRepository("AppBundle:User");
            $user = $userRepo->findOneById($userID);
        }
        elseif ($userID == 0) {
            $user = new User();
        }
        else throw $this -> createNotFoundException();

        if ($user == null) throw $this->createNotFoundException();

        $fosUM = $this -> get('fos_user.user_manager');

        $form= $this -> createForm(UserType::class, $user);
        $form -> handleRequest($request);

        if ($form-> isSubmitted()) {
            if ($form -> isValid()) {

                $previousPassword = $user -> getPassword();

                $currentProject = $user -> getPreferredProject();

                if (empty($form -> get('plainPassword') -> getData())) {
                    $user -> setPassword($previousPassword);
                }


                if ($currentProject != null && $user == $this -> getUser() &&!($user -> getAvailableProjects() -> contains($currentProject))) {

                    $this -> addFlash("warning", "You have switched out of your project by removing yourself from it");
                    $user -> setPreferredProject(null);
                }

                try {
                    $fosUM->updateUser($user);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();

                    $this -> addFlash("notice", "User saved!");
                }
                catch(UniqueConstraintViolationException $ex){
                    $this -> addFlash("error", "E-mail and username must be unique");
                }
                catch(\Exception $ex) {
                    $this->addFlash("error", "Unknown error occured");
                }
            }




            return $this -> redirectToRoute("admin_user_overview");
        }


        return $this->render('admin/modify_user.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'form' => $form -> createView(),
        ]);
    }

    /**
     * @Route("/supervisor/assign_jobs/{id}", name="supervisor_assign_user_jobs")
     */
    public function assignUserJobsAction(Request $request, $id)
    {
        if (!is_numeric($id) || $id < 1) throw $this->createNotFoundException();

        // TODO
        // $this->checkPermissions();

        $repo = $this -> getDoctrine() -> getRepository("AppBundle:User");
        $user = $repo -> findOneById($id);

        if ($user == null) throw $this -> createNotFoundException();

        $form = $this -> createForm(UserJobAssignType :: class, $user, [
            'current_user' => $this -> getUser()
        ]);
        $form -> handleRequest($request);

        if ($form-> isSubmitted()) {
            if ($form -> isValid()) {

                // kontrola, zda nemá uživatel zapsané směny s touto činností
                $shifts = $user -> getShifts();
                $jobs = $user -> getAttending();

                foreach ($shifts as $shift) {
                    if (!($shift -> isInFinalState()) && !($jobs-> contains($shift -> getTimeslot() -> getJob()))) {
                        $this -> addFlash("error", "User still has unfinished shifts with the job ".$shift-> getAttending() -> getName());
                        return $this->redirectToRoute($request -> headers -> get('referer'));
                    }
                }
                $fosUM = $this -> get('fos_user.user_manager');
                $fosUM -> updateUser($user);
                $em = $this -> getDoctrine() -> getManager();
                $em -> persist ($user);
                $em -> flush ();
                $this -> addFlash("notice", "User saved!");

                return $this -> redirectToRoute("supervisor_user_overview");
            }
        }


        return $this->render('supervisor/assign_user_jobs.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'user' => $user,
            'form' => $form -> createView(),
            'return_path' => 'supervisor_user_overview',
        ]);
    }

    /**
     * @Route("/settings/", name="settings")
     */
    public function selfModifyAction(Request $request) {
        $user = $this -> getUser();
        $fosUM = $this -> get('fos_user.user_manager');

        $form= $this -> createForm(UserSelfType::class, $user);
        $form -> handleRequest($request);

        if ($form-> isSubmitted()) {
            if ($form -> isValid()) {

                $previousPassword = $user -> getPassword();

                $currentProject = $user -> getPreferredProject();

                if (empty($form -> get('plainPassword') -> getData())) {
                    $user -> setPassword($previousPassword);
                }



                $em = $this -> getDoctrine() -> getManager();

                try {
                    $fosUM -> updateUser($user);
                    $em->persist($user);
                    $em->flush();
                    $this->addFlash("notice", "Settings saved!");
                }
                catch(UniqueConstraintViolationException $ex) {
                    $this -> addFlash("error", "A person with that e-mail is already registered");
                }
                catch(\Exception $ex) {
                    $this -> addFlash("error", "Unknown error occured");
                }

            }
        }

        return $this -> render("default/settings.html.twig", [
            'form' => $form -> createView(),
        ]);
    }

    /**
     * @Route("/accountant/modify_user_wage/{id}", name="accountant_modify_user_wage")
     */
    public function modifyUserWageAction(Request $request, $id) {
        if (!is_numeric($id) || $id < 1) throw new InvalidInputException("Invalid user ID");
        $user = $this -> getDoctrine() -> getRepository("AppBundle:User") -> findOneById($id);
        if ($user == null) throw new InvalidInputException("User does not exist");

        $form = $this->get('form.factory')->create(UserWageType::class, $user);
        $form -> handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form -> isValid()) {
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em -> persist($user);
                    $em -> flush();
                    $this -> addFlash("notice", "Changes saved!");
                }
                catch(Exception $ex) {
                    $this -> addFlash("error", "Unknown error occured");
                }
            }
        }

        $referer = $request-> headers -> get('referer');
        return new RedirectResponse($referer);

    }
}
