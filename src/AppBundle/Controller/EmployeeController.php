<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Shift;
use AppBundle\Exception\InvalidInputException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Exception\NotImplementedException;
use Doctrine\Common\Collections\ArrayCollection;

class EmployeeController extends Controller
{
    /**
     * @Route("/employee/", name="employee")
     */
    public function employeeAction()
    {
        $this->checkPermissions();

        $shiftRepo = $this -> getDoctrine() -> getRepository("AppBundle:Shift");

        $wdwo = $this -> get('widget_data_employee_week_overview');

        // productivity diagram - week overview

        $raw = $shiftRepo -> countShiftsPerWeekday($this -> getUser());

        $wdwo -> setData($raw);

        $wdwo -> setHiddenDataVal('max', max($raw -> toArray()));

        // productivity diagram - finance overview

        $wdfo = $this -> get('widget_data');

        $wdfo -> setData($shiftRepo -> countSalary($this -> getUser(), new \Datetime()));

        return $this->render('employee/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
            'wdwo' => $wdwo,
            'wdfo' => $wdfo,
        ]);
    }


    /**
     * @Route("/employee/plan/{items}/{view}")
     * @Route("/employee/plan/{items}/{view}/{year}/{month}/{day}", name="employee_plan")
     */
    public function planAction(Request $request, $items='timeslot', $view='agenda', $year = null, $month = null, $day = null) {
        $this -> checkPermissions();

        // Validate fields

        if (empty($day)) $day = date("d");
        if (empty($month)) $month = date("m");
        if (empty($year)) $year = date("Y");
        if (!checkdate($month, $day, $year)) throw new InvalidInputException("Invalid date");
        $date = new \DateTime($year."-".$month."-".$day);

        if (
            !in_array($items, ['timeslot', 'shift']) ||
            !in_array($view, ['agenda', 'calendar', 'daily'])
        ) throw new InvalidInputException("Invalid overview argument");

        $user = $this -> getUser();
        $preferredProject = $user -> getPreferredProject();

        $data = null;
        $start_day = clone $date;
        $end_day = clone $date;
        $render_path = null;
        $return_path = 'employee';

        $tokens = new ArrayCollection();
        $shiftTokens = new ArrayCollection();
        $tokenProvider = $this->get('security.csrf.token_manager');
        $token = $tokenProvider -> refreshToken('ajax');

        if ($items == 'timeslot') {
            if ($view == 'agenda') {
                $start_day = new \DateTime();
                $end_day = null;
            }
            elseif ($view == 'calendar') {
                $start_day -> modify('first day of this month');
                $end_day -> modify('last day of this month');
            }
            elseif ($view == 'daily') {
                // nothing, keeping for readability
            }

            $data = $this -> getDoctrine() -> getRepository('AppBundle:Timeslot') -> findByUser($user, $start_day, $end_day, TRUE, $preferredProject);
        }
        else {
            if ($view == 'agenda') {
                $start_day = null;
                $end_day = null;
            }
            elseif ($view == 'calendar') {
                $start_day -> modify('first day of this month');
                $end_day -> modify('last day of this month');
            }
            elseif ($view == 'daily') {
                // nothing, keeping for readability
            }

            $data = $this -> getDoctrine() -> getRepository('AppBundle:Shift') -> findByUser($user, $start_day, $end_day, TRUE, $preferredProject);

        }

        return $this->render('employee/'.$items.'_overview/'.$view.'_view.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'month' => $month,
            'year' => $year,
            'day' => $day,
            'shifts' => $data,
            'timeslots' => $data,
            'items' => $items,
            'view' => $view,
            'return_path' => $return_path,
            'ajax_token' => $token,
        ]);

    }



    private function checkPermissions() {
        $current_project = $this -> getUser() -> getPreferredProject();
        $current_user = $this -> getUser();
        if (!($current_user -> getAvailableProjects() -> contains($current_project))) {
            $this -> createNotFoundException();
        }
    }
}