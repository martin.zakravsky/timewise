<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 3.4.17
 * Time: 16:46
 */

namespace AppBundle\Controller;


use AppBundle\Exception\CapacityFullException;
use AppBundle\Exception\ConcurrencyException;
use AppBundle\Exception\IllegalShiftSwitchException;
use AppBundle\Exception\NotPermittedException;
use AppBundle\Exception\TimeslotAlreadyUsedException;
use AppBundle\Exception\TimeslotsOverlapException;
use Doctrine\ORM\OptimisticLockException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Exception\InvalidInputException;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Shift;
use AppBundle\Entity\Timeslot;
use Symfony\Component\HttpFoundation\Response;

class ShiftController extends Controller {
    private $ignore_csrf = false;

    /**
     * @Route("/employee/sign_up/{id}/{csrf_token}", name="employee_sign_up")
     */
    public function signUpAction(Request $request, $id, $csrf_token) {
        if (!is_numeric($id) || $id < 1) throw new InvalidInputException;

        $user = $this->getUser();

        $timeslot = $this->getDoctrine()->getRepository("AppBundle:Timeslot")->findOneById($id);

        $this -> validateCsrf($csrf_token);

        if ($timeslot == null ||
            !($user->getAttendingProjects()->contains($timeslot->getProject())) ||
            $timeslot -> getOpen() == false
        ) throw $this->createNotFoundException();


        // Employee can only ask for shifts in his project
        if (!($user -> getAttendingProjects() -> contains($timeslot -> getProject()))) throw new NotPermittedException();

        // Employee cannot request more shifts at the same time
        if (!($timeslot -> getShifts() -> filter(function($shift) use ($timeslot, $user) {
            return (
                $shift -> getUser() == $user &&
                $shift -> getTimeslot() == $timeslot &&
                !($shift -> isInFinalState())
            );
        }) -> isEmpty())) throw new TimeslotAlreadyUsedException();

        $shift = new Shift($user, $timeslot);

        // Check shift overlapping

        $this -> checkOverlapping($shift);

        // Check capacity

        $this -> checkCapacity($shift);

        $em = $this -> getDoctrine() -> getManager();

        $em -> persist($shift);
        $em -> flush($shift);

        $this -> addFlash("notice", "Request complete!");

        return new Response($this -> get('security.csrf.token_manager') -> refreshToken('ajax'));
    }

    /**
     * @Route("/supervisor/confirm/{id}/{csrf_token}", name="supervisor_confirm")
     */
    public function confirmAction(Request $request, $id, $csrf_token) {

        $currentUser = $this -> getUser();

        $em = $this -> getDoctrine() -> getManager();

        $shift = $this -> retrieveAndValidateShift($id);

        $this -> validateCsrf($csrf_token);

        $this -> checkPermissions($currentUser, $shift, $this::AS_SUPERVISOR);

        $this -> switchShift($shift, Shift::CONFIRMED, $this::AS_SUPERVISOR);

        try {
            $em->persist($shift);
            $em->flush();
        }
        catch (OptimisticLockException $ex) {
            throw new ConcurrencyException("Shift has been changed by another user");
        }

        return new Response($this -> get('security.csrf.token_manager') -> refreshToken('ajax'));
    }

    /**
     * @Route("/employee/cancel/{id}/{csrf_token}", name="employee_cancel")
     */
    public function employeeCancelAction(Request $request, $id, $csrf_token) {
        $currentUser = $this -> getUser();
        $em = $this -> getDoctrine() -> getManager();

        $shift = $this -> retrieveAndValidateShift($id);

        $this -> validateCsrf($csrf_token);

        $this -> checkPermissions($currentUser, $shift, $this::AS_EMPLOYEE);

        $this -> switchShift($shift, Shift::CANCELLED, $this::AS_EMPLOYEE);

        try {
            $em->persist($shift);
            $em->flush();
        }
        catch (OptimisticLockException $ex) {
            throw new ConcurrencyException("Shift has been changed by another user");
        }

        return new Response($this -> get('security.csrf.token_manager') -> refreshToken('ajax'));
    }

    /**
     * @Route("/employee/cancel_as_supervisor/{id}/{csrf_token}", name="employee_cancel_as_supervisor")
     */
    public function employeeCancelAsSupervisor(Request $request, $id, $csrf_token) {
        $currentUser = $this -> getUser();
        $em = $this -> getDoctrine() -> getManager();

        $shift = $this -> retrieveAndValidateShift($id);

        $this -> validateCsrf($csrf_token);

        $this -> checkPermissions($currentUser, $shift, $this::AS_BOTH);

        $this -> switchShift($shift, Shift::CANCELLED, $this::AS_BOTH);

        try {
            $em->persist($shift);
            $em->flush();
        }
        catch (OptimisticLockException $ex) {
            throw new ConcurrencyException("Shift has been changed by another user");
        }

        return new Response($this -> get('security.csrf.token_manager') -> refreshToken('ajax'));

    }

    /**
     * @Route("/employee/cancel_by_timeslot/{id}/{csrf_token}", name="employee_cancel_by_timeslot")
     */
    public function employeeCancelByTimeslotAction(Request $request, $id, $csrf_token) {
        $timeslot = $this->retrieveAndValidateTimeslot($id);

        $this -> validateCsrf($csrf_token);

        $shift = $this -> getDoctrine() -> getRepository("AppBundle:Shift") -> findOneBy([
            'timeslot' => $timeslot,
            'user' => $this -> getUser(),
            'state' => [
                Shift::UNCONFIRMED,
                Shift::CONFIRMED,
            ],
        ]);

        $this -> ignore_csrf = true;

        if ($this -> getUser() -> getSupervising() -> contains($shift -> getJob())) {
            return $this->employeeCancelAsSupervisor($request, $shift -> getId(), $csrf_token);
        }
        else {
            return $this->employeeCancelAction($request, $shift -> getId(), $csrf_token);
        }
    }

    /**
     * @Route("/supervisor/cancel/{id}/{csrf_token}", name="supervisor_cancel")
     */
    public function supervisorCancelAction(Request $request, $id, $csrf_token) {
        $currentUser = $this -> getUser();
        $em = $this -> getDoctrine() -> getManager();

        $shift = $this->retrieveAndValidateShift($id);

        $this -> validateCsrf($csrf_token);

        $this -> checkPermissions($currentUser, $shift, $this::AS_SUPERVISOR);

        $this -> switchShift($shift, Shift::CANCELLED, $this::AS_SUPERVISOR);

        try {
            $em->persist($shift);
            $em->flush();
        }
        catch (OptimisticLockException $ex) {
            throw new ConcurrencyException("Shift has been changed by another user");
        }

        return new Response($this -> get('security.csrf.token_manager') -> refreshToken('ajax'));
    }

    /**
     * @Route("/supervisor/mark_as_missed/{id}/{csrf_token}", name="supervisor_mark_as_missed")
     */
    public function supervisorMarkAsMissedAction(Request $request, $id, $csrf_token) {
        $currentUser = $this -> getUser();
        $em = $this -> getDoctrine() -> getManager();

        $shift = $this->retrieveAndValidateShift($id);

        $this -> validateCsrf($csrf_token);

        $this -> checkPermissions($currentUser, $shift, $this::AS_SUPERVISOR);

        $this -> switchShift($shift, Shift::MISSED, $this::AS_SUPERVISOR);

        try {
            $em->persist($shift);
            $em->flush();
        }
        catch (OptimisticLockException $ex) {
            throw new ConcurrencyException("Shift has been changed by another user");
        }

        return new Response($this -> get('security.csrf.token_manager') -> refreshToken('ajax'));
    }

    /**
     * @Route("/supervisor/mark_as_finished/{id}/{csrf_token}", name="supervisor_mark_as_finished")
     */
    public function supervisorMarkAsFinishedAction(Request $request, $id, $csrf_token) {
        $currentUser = $this -> getUser();
        $em = $this -> getDoctrine() -> getManager();

        $shift = $this->retrieveAndValidateShift($id);

        $this -> validateCsrf($csrf_token);

        $this -> checkPermissions($currentUser, $shift, $this::AS_SUPERVISOR);

        $this -> switchShift($shift, Shift::FINISHED, $this::AS_SUPERVISOR);

        try {
            $em->persist($shift);
            $em->flush();
        }
        catch (OptimisticLockException $ex) {
            throw new ConcurrencyException("Shift has been changed by another user");
        }

        return new Response($this -> get('security.csrf.token_manager') -> refreshToken('ajax'));
    }

    /**
     * @Route("/supervisor/reject/{id}/{csrf_token}", name="supervisor_reject")
     */
    public function supervisorRejectAction(Request $request, $id, $csrf_token) {
        $currentUser = $this -> getUser();
        $em = $this -> getDoctrine() -> getManager();

        $shift = $this->retrieveAndValidateShift($id);

        $this -> validateCsrf($csrf_token);

        $this -> checkPermissions($currentUser, $shift, $this::AS_SUPERVISOR);

        $this -> switchShift($shift, Shift::REJECTED, $this::AS_SUPERVISOR);

        try {
            $em->persist($shift);
            $em->flush();
        }
        catch (OptimisticLockException $ex) {
            throw new ConcurrencyException("Shift has been changed by another user");
        }

        return new Response($this -> get('security.csrf.token_manager') -> refreshToken('ajax'));
    }

    /**
     * @Route("/employee/request_cancel/{id}/{csrf_token}", name="employee_request_cancel")
     */
    public function employeeRequestCancel(Request $request, $id, $csrf_token) {
        $currentUser = $this -> getUser();
        $em = $this -> getDoctrine() -> getManager();

        $shift = $this->retrieveAndValidateShift($id);

        $this -> validateCsrf($csrf_token);

        $this -> checkPermissions($currentUser, $shift, $this::AS_EMPLOYEE);

        $this -> switchShift($shift, Shift::REQUIRES_CANCELLATION, $this::AS_EMPLOYEE);

        try {
            $em->persist($shift);
            $em->flush();
        }
        catch (OptimisticLockException $ex) {
            throw new ConcurrencyException("Shift has been changed by another user");
        }

        return new Response($this -> get('security.csrf.token_manager') -> refreshToken('ajax'));
    }

    /**
     * @param $id
     * @return Shift
     * @throws InvalidInputException
     */
    private function retrieveAndValidateShift($id) {
        if (!is_numeric($id) || $id < 1) throw new InvalidInputException("Invalid shift id");

        $shift = $this -> getDoctrine() -> getRepository("AppBundle:Shift") -> findOneById($id);

        if ($shift == null) throw new InvalidInputException("Shift does not exist");

        return $shift;
    }
    /**
     * @param $id
     * @return Timeslot
     * @throws InvalidInputException
     */
    private function retrieveAndValidateTimeslot($id) {
        if (!is_numeric($id) || $id < 1) throw new InvalidInputException("Invalid timeslot id");

        $timeslot = $this -> getDoctrine() -> getRepository("AppBundle:Timeslot") -> findOneById($id);

        if ($timeslot == null) throw new InvalidInputException("Timeslot does not exist");

        return $timeslot;
    }


    private function checkPermissions($user, $shift, $role) {
        if ($user == null || $shift == null) throw new \LogicException("Null shift or user passed to permission check");

        if ($role == $this::AS_EMPLOYEE || $role == $this::AS_BOTH) {
            if ($shift -> getUser() != $user) throw new NotPermittedException("Employee can only operate on his own shifts");
        }

        if ($role == $this::AS_SUPERVISOR || $role == $this::AS_BOTH) {
            if (!($user -> getSupervising() -> contains($shift -> getTimeslot() -> getJob()))) throw new NotPermittedException("Supervisor can only operate on his jobs' shifts");
        }

    }

    private function switchShift(&$shift, $targetState, $role) {
        $currentState = $shift -> getState();

        switch ($targetState) {
            case Shift::UNCONFIRMED:

                // Does not make sense to validate

                break;
            case Shift::CONFIRMED:
                if (!in_array($currentState, [
                    Shift::UNCONFIRMED,
                    Shift::REQUIRES_CANCELLATION,
                ])) throw new IllegalShiftSwitchException("Illegal shift switch to 'confirmed'");
                break;
            case Shift::REQUIRES_CANCELLATION:
                if ($currentState != Shift::CONFIRMED) throw new IllegalShiftSwitchException("Illegal shift switch to 'requires cancellation'");
                break;
            case Shift::CANCELLED:
                if ($currentState != Shift::UNCONFIRMED && $role == $this::AS_EMPLOYEE) throw new IllegalShiftSwitchException("Employee cannot himself cancel a shift, that is not unconfirmed");

                if (!in_array($currentState, [
                    Shift::UNCONFIRMED,
                    Shift::CONFIRMED,
                    Shift::REQUIRES_CANCELLATION,
                ])) throw new IllegalShiftSwitchException("Illegal shift switch to 'cancelled'");

                break;
            case Shift::REJECTED:
                if ($currentState != Shift::UNCONFIRMED) throw new IllegalShiftSwitchException("Illegal shift switch to 'rejected'");
                break;
            case Shift::MISSED:
                if (!in_array($currentState, [
                    Shift::CONFIRMED,
                    Shift::FINISHED,
                ])) throw new IllegalShiftSwitchException("Illegal shift switch to 'missed'");
                if ($shift -> getTimeslot() -> getDate() > new \DateTime()) throw new IllegalShiftSwitchException("Marking a shift as missed is possible only if the shift is in the past");
                break;
            case Shift::FINISHED:
                if (!in_array($currentState, [
                    Shift::CONFIRMED,
                    Shift::MISSED,
                ])) throw new IllegalShiftSwitchException("Illegal shift switch to 'finished'");
                if ($shift -> getTimeslot() -> getDate() > new \DateTime()) throw new IllegalShiftSwitchException("Marking a shift as finished is possible only if the shift is in the past");
                break;
            default:
                throw new \LogicException("Unknown target state passed to shift switch validator");
        }

        $shift -> setState($targetState);

        if ($role == $this::AS_SUPERVISOR) $this -> get('message_service') -> shiftChangeNotify($shift, $this -> getUser(), $targetState);
    }

    const AS_EMPLOYEE = 0;
    const AS_SUPERVISOR = 1;
    const AS_BOTH = 2;

    private function checkOverlapping(Shift $testedShift) {
        $project = $testedShift -> getJob() -> getProject();

        if ($project -> getAllowsOverlappingShifts()) return;

        $user = $testedShift -> getUser();

        $relevantShifts = $this -> getDoctrine() -> getRepository("AppBundle:Shift") -> findByUser($user, null, null, true, $project);

        foreach ($relevantShifts as $shift) {
            if (in_array($shift -> getState(), [
                Shift::CANCELLED,
                Shift::REJECTED
            ])) continue;
            if ($shift -> getTimeslot() -> overlapsWith($testedShift -> getTimeslot())) throw new TimeslotsOverlapException("This shift overlaps with your other shift");
        }
    }

    private function checkCapacity(Shift $shift) {
        if ($shift -> getTimeslot() -> isFull()) throw new CapacityFullException("Capacity is already full");
    }

    private function validateCsrf($token) {
        if (!($this -> isCsrfTokenValid('ajax', $token))) {
            throw new InvalidInputException("CSRF protection violated");
        }
    }
}