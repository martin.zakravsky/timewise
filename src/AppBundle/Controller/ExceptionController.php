<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Shift;
use AppBundle\Exception\InvalidInputException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Exception\NotImplementedException;
use Doctrine\Common\Collections\ArrayCollection;

class ExceptionController extends Controller
{
    public function handleExceptionAction(Request $request, FlattenException $exception, $logger) {
        return new \Symfony\Component\HttpFoundation\Response($exception -> getMessage());
    }
}